import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";
import config from "../config/config";

class AuthController {
  static login = async (req: Request, res: Response): Promise<Response> => {
    //Check if username and password are set
    const { username, password } = req.body;
    if (!(username && password)) {
      return res.status(400).send();
    }

    //Get user from database
    const userRepository = getRepository(User);
    let user;
    try {
      user = await userRepository.findOneOrFail({ where: { username } });
    } catch (error) {
      return res.status(401).send();
    }

    //Check if encrypted password match

    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      return res.status(401).send();
    }

    //Sing JWT, valid for 1 hour
    const token = jwt.sign({ userId: user.id, username: user.username },
      config.jwtSecret,
      { expiresIn: "1h" }
    );

    //Send the jwt in the response
    return res.send(token);
  };

  static changePassword = async (req: Request, res: Response): Promise<Response> => {
    //Get ID from JWT
    const id = res.locals.jwtPayload.userId;

    //Get parameters from the body
    const { oldPassword, newPassword } = req.body;
    if (!(oldPassword && newPassword)) {
      return res.status(400).send();
    }

    //Get user from the database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      return res.status(401).send();
    }

    //Check if old password matches
    if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
      return res.status(401).send();
    }

    //Validate de model (password length)
    user.password = newPassword;

    const errors = await validate(user);
    if (errors.length > 0) {
      return res.status(400).send(errors);
    }
    //Hash the new password and save
    user.hashPassword();

    await userRepository.save(user);

    return res.status(204).send();
  };
}
export default AuthController;
